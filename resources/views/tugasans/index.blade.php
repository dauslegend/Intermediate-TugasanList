@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Tugasan
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Task Form -->
                    <form action="{{ url('tugasan') }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <!-- Task Name -->
                        <div class="form-group">
                            <label for="tugasan-name" class="col-sm-3 control-label">Tugasan</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="tugasan-name" class="form-control" value="{{ old('tugasan') }}">
                            </div>
                        </div>

                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Add Tugasan
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Current Tasks -->
            @if (count($tugasans) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Current Tugasans
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped tugasan-table">
                            <thead>
                                <th>Tugasan</th>
                                <th>&nbsp;</th>
                            </thead>
                            <tbody>
                                @foreach ($tugasans as $tugasan)
                                    <tr>
                                        <td class="table-text"><div>{{ $tugasan->name }}</div></td>

                                        <!-- Task Delete Button -->
                                        <td>
                                            <form action="{{url('tugasan/' . $tugasan->id)}}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button type="submit" id="delete-tugasan-{{ $tugasan->id }}" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection