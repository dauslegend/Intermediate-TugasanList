<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('/')
             ->see('Welcome');

    }

    public function testLinkExample()
    {
        $this->visit('/')
             ->click('Register')
             ->seePageIs('register')
             ->click('Login')
             ->seePageIs('login')
             ->click('Home')
             ->seePageIs('/login');
    }

    public function testNewUserRegistration()
    {  
        $this->visit('/register')          
             ->type('lolsdfds', 'name')
             ->type('blkasdfs@gmail.com', 'email')
             ->type('123456', 'password')
             ->type('123456', 'password_confirmation')
             ->press('Register')
             ->seePageIs('/tugasans');
             

    }

    public function testLogin()
    {  
        $this->visit('/login')          
             ->type('blkasdfs@gmail.com', 'email')
             ->type('123456', 'password')
             ->check('remember')
             ->press('Login')
             ->seePageIs('/tugasans');
             

    }

    public function testLogout()
    {
        $this->visit('/logout')
             ->seePageIs('/');
    }

    

}
