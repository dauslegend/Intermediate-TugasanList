<?php

namespace App\Http\Controllers;

use App\Tugasan;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\TugasanRepository;
// use App\Http\Requests;

class TugasanController extends Controller
{

    public function __construct(TugasanRepository $tugasans)
    {
        $this->middleware('auth');

        $this->tugasans = $tugasans;
    }


   	 public function index(Request $request)
	{
    	$tugasans = $request->user()->tugasans()->get();

	    return view('tugasans.index', [
	        'tugasans' => $this->tugasans->forUser($request->user()),
	    ]);
	}

	public function store(Request $request)
	{
    	$this->validate($request, [
        'name' => 'required|max:255',
    ]);

    $request->user()->tugasans()->create([
        'name' => $request->name,
    ]);

    	return redirect('/tugasans');
	}

	public function destroy(Request $request, Tugasan $tugasan)
	{
	    $this->authorize('destroy',$tugasan);

	    $task->delete();

    	return redirect('/tasks');
	}
}
