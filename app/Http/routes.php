<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () 
{

	Route::get('/', function () 
	{
	    return view('welcome');
	})->middleware('guest');




	Route::get('/tugasans', 'TugasanController@index');
	Route::post('/tugasan', 'TugasanController@store');
	Route::delete('/tugasan/{tugasan}', 'TugasanController@destroy');

	Route::auth();

});
/**
 * Destroy the given task.
 *
 * @param  Request  $request
 * @param  Task  $task
 * @return Response
 */
// public function destroy(Request $request, Tugasan $tugasan)
// {
//     //
// }

