<?php

namespace App\Repositories;

use App\User;

class TugasanRepository
{
    /**
     * Get all of the tasks for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    public function forUser(User $user)
    {
        return $user->tugasans()
                    ->orderBy('created_at', 'asc')
                    ->get();
    }
}