<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Tugasan extends Model
{
    protected $fillable = ['name'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
