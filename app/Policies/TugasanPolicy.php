<?php

namespace App\Policies;

use App\User;
use App\Tugasan;
use Illuminate\Auth\Access\HandlesAuthorization;

class TugasanPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given user can delete the given task.
     *
     * @param  User  $user
     * @param  Task  $task
     * @return bool
     */
    public function destroy(User $user, Tugasan $tugasan)
    {
        return $user->id === $tugasan->user_id;
    }
}